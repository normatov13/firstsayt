<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class postController extends Controller
{
    public function create()
    {
        // return view('post.create');
    }

    public function store(Request $request)
    { 
        $request->validate([
            
            // 'title' => 'required|unique:posts|max:255',
           
            'names' => 'required|min:5',
            'emails' => 'required|min:4',
            // 'author.parols' => 'nullable|date'
        
        ]);

    

    Validator::make($request->all(), [
        'names' => 'required|min:5',
        'emails' => 'required|min:4',
    ])->validateWithBag('post');
    
}
}