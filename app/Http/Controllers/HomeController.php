<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use DB;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Session;
use Auth;
use Illuminate\Support\Facades\Storage;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
       {    
        if(intval(Auth::user()->admin) === 1){ 
        $tabless=DB::table('users')->join('tip_us','tip_us.id','=','users.tip','left')
        ->select('users.*','tip_us.ism as ism');
        $val=10;
        $searchval=trim(request()->get("search"));
        
       $clear=(int)\request()->get('clear');
       $drop=(int)\request()->get('drop');
       

     if(empty( $searchval)){
      if($clear===1){
        $searchval=Session::put('search','');}      
      else { $searchval=Session::get('search');}   
     }
      else{ Session::put('search',$searchval);}
      
     if(!empty($searchval)){
        $tabless->where("name","like",'%'.$searchval.'%')->paginate($val); }



        if( $drop===0){
          if($clear===1){
            Session::put('drop','');}

        else { $drop=Session::get('drop');}
       
         }
          else{
         Session::put('drop',$drop);}         
          if($drop>0){
         $tabless->where('tip','=',$drop)->get();
          }else(redirect('/home'));
        
            $tables=$tabless->paginate($val);
            $page=(int)\request()->get('page');
            if($page==0)$page=1;
             $start=($page-1)*$val;
             $uTypes=DB::table('tip_us')->get();
     
            return view('home',['tables'=>$tables,
            'start'=>$start,
            'pageid'=>$page,
            'uTypes'=>$uTypes,
            'searchval'=>$searchval,
            'drops'=>$drop
            ]);}
            else{
              return view('Homes');
            }
       }
   


         public function deletes(Request $request, $id){
        $user=new User;
        $user->where('id',$id)->delete();
        $request->session()->flash('success',$id.'-ID ga Ega Satr Muvaffaqiyatli O`chirildi');
        $page=request()->get('page');
        if($page==0)$page=1;
        return redirect('/home/?page='.$page);
        }



                   public function editess($id){
            //   $usert = User::findOrFail($id);
                   $usert=new User;                 
                   $usert=$usert->where('id',$id)->first();
                    return view('/Edit',compact('usert'));
                }


        public function updetes($id){
        $user=new User;
         $page=request()->get('page');
        $user->where('id',$id)->update(['name'=>$_GET['ism'],'email'=>$_GET['email']]);
        return redirect('/home/?page='.$page);
    }
   

   public function pluss(Request $request)
   {
   $yoz=new User;

   $request->validate([
    'name'=>'required|min:3',
    'email'=>'required|email',
    'parol'=>'required|min:6|integer',
   ],
    $message=[
     'email.required'=>'Email kiritilmadi!!!'
   ]);
 
   $vali=DB::table('users')->where('email',$_POST['email'])->first('email');
   if(!($vali===null)){
     $messag="Ushbu Email Allaqchon Ro'yxatga Olingan";
     return view('plus',['messag'=>$messag]);
   }  
   else{
    $fayl=$request->file('profile')->storeAs('fayls',date("Y_h_i_s_A").'.jpg','public');
   
    $password = Hash::make($_POST['parol']);
    DB::insert('insert into users (name,email,password,rasm) values (?,?,?,?)',
     [$_POST['name'],  $_POST['email'],$password,$fayl]);
  
    return redirect('/home');}
   
   }
     
      public function profils(Request $request, $id){

        $fus=$request->file('profil')->storeAs('image',date("Y_h_i_s_A").'.jpg','public');
       DB::table('users')->where('id',$id)->update(['pprofil'=>$fus]);
         return redirect('/home');
     
      }
    
      public function exsel(){
        
        return Excel::download(new UsersExport, 'users.xlsx');

      }
       
       
       
        public function Medialar(Request $request, $id){
          $rasm=DB::table('photo')->where('tipid',$id)->get();
        //  return view('Media',['rasm'=>$rasm]);

       $video=DB::table('video')->where('tipid',$id)->get();
      // return view('Media',['video'=>$video]);
      
      $file=DB::table('file')->where('tipid',$id)->get();
    // $users=array_merge_recursive($rasm,$video,$file);
    //  dd($users);
    //  return view('Media',['file'=>$file]);
        //$users = DB::table('photo')->get();
        // ->join('file', 'video.id', '=', 'file.id','left')
        // ->join('photo', 'video.id', '=', 'photo.id','left')
        // ->select('video.*', 'file.file as files', 'photo.rasm as rasms')
        // ->get();
        // $first = DB::table('video')
        //     ->whereNull('video');
        //     $users = DB::table('users')
        //     ->whereNull('last_name')
        //     ->union($first)
        //     ->get();
        // dd($users);
          // $users = DB::table('photo')->where('tipid',$id)->get();
          return  view('Media',['rasm'=>$rasm,'userid'=>$id,'video'=>$video,'file'=>$file]);
        }





        public function eshikmed($id){
         
          return  view('medin',['id'=>$id]);
        }
    





        public function Media(Request $request, $id){
    
         // dd($request->hasfile('media'));
      if($request->hasfile('media')){
        $med=$request->file('media')->getClientOriginalName();
       $fors=explode(".",$med);
       $fors=end($fors);
       $fors=strtolower($fors);
    
     
       $a=array("jpg","png","jpeg");
       $b=array("mp4");
       $c=array("doc","xlsx","pdf");
      if(in_array($fors,$a)){
        // dd($id);
     $man=$request->file('media')->storeAs('photo',date("Y_h_i_s_A").'.'.$fors,'public');
     DB::table('photo')->insert(['tipid'=>$id,'rasm'=>$man]);
 
      }
       
      elseif(in_array($fors,$b)){
        $man=$request->file('media')->storeAs('video',date("Y_h_i_s_A").'.'.$fors,'public');
        DB::table('video')->insertGetId(['tipid'=>$id,'video'=>$man,'oname'=>$med]);
    
    }
        
     elseif(in_array($fors,$c)){
      $man=$request->file('media')->storeAs('file',date("Y_h_i_s_A").'.'.$fors,'public');
      DB::table('file')->insertGetId(['tipid'=>$id,'file'=>$man,'oname'=>$med]);
     
     }
   
     return  redirect('/media/file/'.$id.'/alone');
    }
    else{
      return  redirect('/media/file/{id}/plus');
    }
      
      }

      public function delphoto($userid, $id){
    
        $rasm=DB::table('photo')->where('id',$id)->first();
        if(!empty($rasm->rasm)){
        DB::table('photo')->where('id',$id)->delete();
        if(storage::disk('public')->exists('/photo/',$rasm->rasm)){
        Storage::disk('public')->delete('/photo/',$rasm->rasm);}
        }

     return redirect('/media/file/'.$userid.'/alone');
    }
    
    public function delvideo($userid, $id){
    
      $video=DB::table('video')->where('id',$id)->first();
      if(!empty($video->video)){
      DB::table('video')->where('id',$id)->delete();
      if(storage::disk('public')->exists('/video/',$video->video)){
      Storage::disk('public')->delete('/video/',$video->video);}
      }

   return redirect('/media/file/'.$userid.'/alone');
  }
  
  public function delfile($userid, $id){
    
    $file=DB::table('file')->where('id',$id)->first();
    if(!empty($file->file)){
    DB::table('file')->where('id',$id)->delete();
    if(storage::disk('public')->exists('/file/',$file->file)){
    Storage::disk('public')->delete('/file/',$file->file);}
    }

 return redirect('/media/file/'.$userid.'/alone');
}
    }
