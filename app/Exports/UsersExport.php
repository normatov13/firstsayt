<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromView;
use illuminate\Contracts\view\view;
use Maatwebsite\Excel\Concerns\FromCollection;

// class UsersExport implements FromCollection
// {
//     /**
//     * @return \Illuminate\Support\Collection
//     */
//     public function collection()
//     {
//         return User::all();
//     }
// }
 class UsersExport implements Fromview
 {
 
    public function view():view
 {
     return view('users-excel',[
        'tables'=>User::all()
      ]);
 }
 


 }


