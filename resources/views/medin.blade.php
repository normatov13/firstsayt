@extends('layouts.app')



@section('content')


 
              <!--card start-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                 <div class="card-header">
                
                 <h3 class="offset-2">   Ushbu Panelda Siz foydalanuvchilar Uchun Saytga Video va Fayllar Yuklashingiz Mumkin</h3>

                 </div>
      
              <div class="card-body">   
         
              </br>
              <div class="row justify-content-center">
                 <form action="/medios/files/{{$id ?? ''}}/store" method="post"  enctype="multipart/form-data">
                      <div class="form-group">
                           <input type="file"  value="Fayl yuklash"  name="media" multiple>
                                </div>
                                     <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Yuklash" >
                                <h3>{{$mess ?? ''}}</h3>  </div>
                              {{ csrf_field() }}
                               
                          </form>
                        
                          </div>
            </div>
             </div>
        </div>
    </div>
</div>

@endsection