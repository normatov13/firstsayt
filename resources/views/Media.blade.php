@extends('layouts.app')



@section('content')

              <!--card start-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
<div class="card">
   <div class="card-header">
        <div class="form-inline">
            <h5 class="offset-2"> Medios and File</h5>
                <div class="offset-6">
               
                    <a href="/media/file/{{$userid ?? ''}}/plus" class="btn btn-success ml-2 ">Add Media</a> 
                       <a href="/home" class="btn btn-dark ml-2 ">Users</a>       
                   </div>  
                       </div>
                          </div>

                      <div class="card-body">   
               
           <table class="table ">
       <thead class="thead-dark">
    <tr>
   <th scope="col"> Rasmlar </th>
        <th scope="col"> Actions </th>
          </tr>
             </thead>  
                <tbody >
     @foreach($rasm as $ras)

    <tr>
         <th scope="row">   <a href="/storage/{{$ras->rasm}}" target="_blank">
         <img src="/storage/{{$ras->rasm}}" height="60px" ></a></th>

            <td>                
            <a href="/media/file/{{$userid ?? ''}}/del/{{$ras->id}}">  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                   <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                      <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                         </svg>Delete</button></a></td>
                           </tr>
                             
                                 @endforeach
                                 </tbody>
                                     </table>
                                   <hr> </br></br>

                                <table class="table ">
                          <thead class="thead-dark">
                       <tr>
                 <th scope="col"> Fayllar </th>
          <th scope="col"> Actions </th>
       </tr>
   </thead>  
              <tbody >
   @foreach($file as $fil)

 <tr>
      <th scope="row"><a href="/storage/{{$fil->file}}" height="60px" >{{$fil->oname}}</a></th>
           <td> 
           <a href="/media/file/{{$userid ?? ''}}/delfil/{{$fil->id}}">        
                 <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                       <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                     </svg>Delete</button></a></td>
               </tr>
    @endforeach
    </tbody>
    </table>
  <hr> </br></br>
          <table class="table ">
                <thead class="thead-dark">
                    <tr>
                       <th scope="col">Videolar</th>
                           <th scope="col"> Actions </th>
                                </tr>
                                  </thead>  
                        <tbody >
    @foreach($video as $vid)
 
       <tr>
      
            <th scope="row"> <a href="/storage/{{$vid->video}}" target="_blank"><video src="/storage/{{$vid->video}}" height="60px" ></a></br>
            <p>{{$vid->oname}}</p>
            </th>
                <td>             
                <a href="/media/file/{{$userid ?? ''}}/delvid/{{$vid->id}}">  
                   <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                              </svg>Delete</button></a> 
                              </td>
                                   </tr>
                        
     @endforeach
         </tbody>
                  </table>


 
               </div>
             </div>
        </div>
    </div>
</div>

@endsection