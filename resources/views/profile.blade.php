<div class="bg-success">
@extends('layouts.app')


@section('content')

<div class="row justify-content-center">
<div class="col-9">
<div class="card ">

<div class="card-header">
<div class="col-lg-4  offset-2">
<div class="form-inline">

      <img  src="/storage/{{$rasm->pprofil}}" class="rounded-circle" alt="image" width="100" height="100"><h3> {{$nam->name}}'s Profile</h3>
</div></div></div>
<div class="card-body ">
<div class="container">

   <h5>You can Upload your profile Picture in this</h5></br>
<form action="/profil/{{$id}}" method="post"  enctype="multipart/form-data">
    <div class="form-group">
    <input type="file"  value="Fayl yuklash"  name="profil">
  </div>
  <div class="form-group">
<input type="submit" class="btn btn-primary" value="Yuklash" >

{{ csrf_field() }}

</form>
</div></div>
<h4 class="row justify-content-center">Sozlamalar</h4>
</br>
<ul class="list-group list-group-flush">
  <li class="list-group-item">Privacy and policies</li>
  <li class="list-group-item">Kurs tafsilotlari</li>
  <li class="list-group-item">Hisobotlar</li>
  <li class="list-group-item">Kurslarni Tanlash</li>
  <li class="list-group-item">Login activity</li>
</ul>
</div>
</div>
</div>
</div>
@endsection