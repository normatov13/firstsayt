@extends('layouts.app')



@section('content')
<div class="bg-secondary">

 
              <!--card start-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                 <div class="card-header bg-dark text-light pb-4"> 
                      <!-- <div  class="inform-inline pb=o"> -->
                      <div class="form-inline">  
                      <a type="button" class="btn btn-danger text-white mr-2">Delete check</a>
                      <a href="/pluss" class="btn btn-success">Add User</a>
                      <a href="/exsel/export" class="btn btn-primary ml-2 ">Export Exsel</a>
                    
               <div class="offset-3">
               <form method="get" action="/home" >
                                          
       <select  name="drop" class="custom-select bg-secondary text-light mr-3">
       <option value= "-1" >All Users</option>
    
      @foreach ($uTypes as $types)
       <option value ="{{$types->id}}"
      
       @if($drops===$types->id)
       selected
       @endif
       > {{$types->ism}} </option>
       @endforeach 
       </select>
              <input type="text" name="search" class="form-control" placeholder="Search..." value="{{$searchval ?? ''}}">
              <button type="submit" class="btn btn-light mr-2"><svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
              <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
               </svg></button></form>
               </div>   
              
              <a href="/home?clear=1" class="btn btn-danger">Clear</a>
              </div> 
                 
                   </div>
                      
    
                   

                   @if(Session::has('success'))  
                   <div class="alert alert-success text-dark" role="alert">
                <h3 class="row justify-content-center">   {{Session::get('success')}}</h3></div>
                    @endif
                         
                         
                         
                          <div class="card-body  ">
                            @if (session('status'))
                                  <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                      
                    @endif
                    <h4 class="offset-4">Ro`yhatdan o`tganlar jadvali</h4>
                    <table class="table table-bordered">
                   <thead >
                    <tr>
                    <th><svg class="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
</svg></th>
                    <th>No</th>
                   <th>ID</th>
                   <th>tip</th>
                   <th>Name</th>
                   <th>Email</th>
                   <th>Picture</th>
                   <th>Action</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($tables as $qiymat)


                

                 
                  <tr>
                  <td><form action="/Del" method="post"><input type="checkbox" name="del"></form></td>
                  <td>{{++$start}}</td>
                  <td>{{$qiymat->id}}</td>
                  <td>{{$qiymat->ism}}</td>
                  <td>{{$qiymat->name}}</td>
                  <td>{{$qiymat->email}}</td>
                  <td>
                  <a href="/storage/{{$qiymat->rasm}}" target="_blank">
                  <img src="/storage/{{$qiymat->rasm}}" height="60px" alt="imge">
                  </a>
                  </td>
                 <td> 
                 <a href="/media/file/{{$qiymat->id}}/alone" class="btn btn-success ml-2 ">Media</a>        
                 <a href="/edites/{{$qiymat->id}}?page={{$pageid}}"><button type="button" class="btn btn-primary"  ><svg class="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg>Edit</button></a>
                
                
 <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{$qiymat->id}}"><svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
</svg>Delete</button>
                       </td>
                  </tr>




<!-- Modal -->
<div class="modal fade" id="exampleModal{{$qiymat->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure???</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="/deletess/{{$qiymat->id}}?page={{$pageid??''}}"><button type="button" class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>

               @endforeach
                   </tbody>
                  </table>

                  {{ $tables->links() }}
        
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection

