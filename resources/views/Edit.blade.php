@extends('layouts.app')

@section('content')

<div class="col-md-8 container">
<div class="card">
         
                <div class="card-header">Admin O`zgartirishlari</div>
                <div class="card-body">
<div class=" row justify-content-center">

<form class="col-4" method="get" action="/updet/{{$usert->id}}?page=$page">
  <div class="form-group justify-content-center">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control " name="ism" aria-describedby="emailHelp" value="{{$usert->name}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Email</label>
    <input type="email" class="form-control" name="email"  value="{{$usert->email}}">
  </div>
  
  <button type="submit" class="btn btn-primary"><svg class="bi bi-archive-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M12.643 15C13.979 15 15 13.845 15 12.5V5H1v7.5C1 13.845 2.021 15 3.357 15h9.286zM6 7a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1H6zM.8 1a.8.8 0 0 0-.8.8V3a.8.8 0 0 0 .8.8h14.4A.8.8 0 0 0 16 3V1.8a.8.8 0 0 0-.8-.8H.8z"/>
</svg>Saqlash</button>
</form>
</div>
</div>
</div>
</div>

@endsection