<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Homes');
});

Auth::routes();




Route::get('/hot',function(){ return view('Homes');});

Route::any('/home', 'HomeController@index');

// Route::get('/search','HomeController@searches');

Route::get('/deletess/{id}', 'HomeController@deletes');

Route::get('/edites/{id}','HomeController@editess');

Route::get('/updet/{id}','HomeController@updetes');

Route::get('/userfaker',function(){
    $factory=Faker\Factory::create();
     for($i=1;$i<=30;$i++){
    DB::insert("INSERT INTO users(name,email,password) VALUES(?,?,?)",[$factory->name,$factory->unique()->email,"12345"]);
     }
});
///////////////////////////////////
Route::get('/pluss',function(){
    return view('plus');
});
Route::post('/plusss','HomeController@pluss');
///////////////////////////////
Route::get('/pro/{id}',function($id){
    $nam=DB::table('users')->where('id',$id)->first('name');
    $rasm=DB::table('users')->where('id',$id)->first('pprofil');
    return view('profile',['id'=>$id,'nam'=>$nam,'rasm'=>$rasm]);
});
Route::post('/profil/{id}','HomeController@profils');
/////////////export exsel

Route::get('/exsel/export','HomeController@exsel');

////// Media and File yuklash

Route::get('/media/file/{id}/alone','HomeController@Medialar');
Route::get('/media/file/{id}/plus','HomeController@eshikmed');
Route::post('/medios/files/{id}/store','HomeController@Media');
Route::get('/media/file/{userid}/del/{id}','HomeController@delphoto');
Route::get('/media/file/{userid}/delfil/{id}','HomeController@delfile');
Route::get('/media/file/{userid}/delvid/{id}','HomeController@delvideo');






Route::get('/shab',function(){ return view('shablon');});
Route::get('post/create', 'PostController@create');
Route::post('/post', 'PostController@store');